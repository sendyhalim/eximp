var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var livereload = require('gulp-livereload');
var prefixer = require('gulp-autoprefixer');

var paths = {
  libraries: {
    css: [
      'bower_components/bootstrap/dist/css/bootstrap.min.css',
      'bower_components/font-awesome/css/font-awesome.min.css'
    ],
    js: [
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/bootstrap/dist/js/bootstrap.min.js'
    ]
  },
  watch: {
    files: [
      'assets/less/**/*.less',
      'public/**/*.html'
    ]
  },
  source: {
    less: {
      init: ['assets/less/init.less'],
      files: ['assets/less/**/*.less']
    }
  },
  output: {
    cssDirectory: 'public/dist',
    cssFilename: 'styles.css',
    cssLibrariesName: 'lib.css',

    jsDirectory: 'public/dist',
    jsFilename: 'app.js',
    jsLibrariesName: 'lib.js'
  }
};

gulp.task('less', function (cb) {
  var lessPipe = less().on('error', function(err) {
    console.log(err.toString());
  });

  var prefixerPipe = prefixer({
    browsers: ['> 1%'],
    cascade: false
  });

  return gulp.src(paths.source.less.init)
             .pipe(lessPipe)
             .pipe(concat(paths.output.cssFilename))
             .pipe(prefixerPipe)
             .pipe(gulp.dest(paths.output.cssDirectory))
             .pipe(livereload());
});

gulp.task('concat-css-lib', function(cb) {
  return gulp.src(paths.libraries.css)
             .pipe(concat(paths.output.cssLibrariesName))
             .pipe(gulp.dest(paths.output.cssDirectory));
});

gulp.task('concat-js-lib', function(cb) {
  return gulp.src(paths.libraries.js)
             .pipe(concat(paths.output.jsLibrariesName))
             .pipe(gulp.dest(paths.output.jsDirectory));
});


gulp.task('watch', function() {
  livereload.listen();
  gulp.watch(paths.watch.files, ['build']);
});

gulp.task('build', [
  'less',
  'concat-css-lib',
  'concat-js-lib'
]);

gulp.task('default', ['build']);
