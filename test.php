<?php

class Human {
    public $firstName = "Sendy";
    public $lastName = "Halim";

    public function getFullName() {
        return $this->firstName . ' ' . $this->lastName;
    }
}

$sendy = new Human();

// var_dump($sendy);

echo $sendy->getFullName(); // Sendy Halim

