$(document).ready(function() {
  var $loaded = $('#loaded');
  var target = $loaded.attr('data-target');

  $loaded.load(target, function () {
    console.log('Loaded:', target);
  });
});
